import json
import boto3
import psycopg2
import psycopg2.extras

client = boto3.client('secretsmanager')

# The name or ARN of the secret
secret = "<arn_of_secret_manager"

# Retrieve secret
get_secret_value_response = client.get_secret_value(SecretId=secret)

# Parse the JSON from the secret
if 'SecretString' in get_secret_value_response:
    secret = json.loads(get_secret_value_response['SecretString'])
else:
    binary_secret_data = get_secret_value_response['SecretBinary']
    
# Extract the database credentials
    
username = secret['username']
password = secret['password']
endpoint = secret['endpoint']
database_name = secret['databasename']
port = secret['port']

# Initialize a database connection (assuming PosgreSQL)
def get_db_connection():
    return psycopg2.connect(host=endpoint, dbname=database_name, user=username, password=password, port=port )

def lambda_handler(event, context):
    sns_message = json.loads(event['Records'][0]['body'])
    parsed_message = json.loads(sns_message['Message'])
    env = parsed_message['mail']['tags']['env'][0]
  #  Open a new database connection
    with get_db_connection() as conn:
        with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
            # Extract relevant information
            event_type = parsed_message['eventType']
            message_id = parsed_message['mail']['messageId']
            destinations = ', '.join(parsed_message['mail']['destination'])
            user_id = parsed_message['mail']['tags']['user_id'][0]
            reciever_id = parsed_message['mail']['tags'].get('reciever_id', [None])[0]
            newsletter_id = parsed_message['mail']['tags'].get('newsletter_id', [None])[0]
            announcement_id = parsed_message['mail']['tags'].get('announcement_id', [None])[0]
            campaign_type = parsed_message['mail']['tags']['campaign_type'][0]
            try:
                user_id = int(user_id)
                reciever_id = int(reciever_id) if reciever_id is not None else None
                newsletter_id = int(newsletter_id) if newsletter_id is not None else None
                announcement_id = int(announcement_id) if announcement_id is not None else None
            except ValueError:
                print("Conversion error: user_id or reciever_id is not a valid integer")
                return {"status": "Error", "message": "Invalid user_id or reciever_id"}


            if event_type == "Send":
                # Insert new record
                sql = """
                INSERT INTO public."Campaignanalytics" (user_id, reciever_id, student_email, campaign_type, message_id, newsletter_id, announcement_id, is_sent, "createdAt", "updatedAt")
                VALUES (%s, %s, %s, %s, %s, %s, %s, true, NOW(), NOW())
                """
                cur.execute(sql, (user_id, reciever_id, destinations, campaign_type, message_id, newsletter_id, announcement_id ))
                conn.commit()
                print(f"New record created for 'Send' event in campaign_analytics table")

            event_to_column_map = {
                "Delivery": "is_delivered",
                "Open": "is_opened",
                "Click": "is_clicked",
                "Bounce": "is_bounce",
                "Reject": "is_rejected",
                "Complaint": "is_complained"
            }

            if event_type in event_to_column_map:
                field_to_update = event_to_column_map[event_type]
                update_sql = f"""
                UPDATE public."Campaignanalytics"
                SET {field_to_update} = true , "updatedAt" = NOW()
                WHERE user_id = %s AND campaign_type = %s AND message_id = %s AND student_email = %s 
                """
                cur.execute(update_sql, (user_id, campaign_type, message_id, destinations))
                conn.commit()
                # print(f"Updated record for '{event_type}' event in analytics table in {env} environment")
                
                if cur.rowcount == 0:
                    # If no rows were updated, verify if the record exists
                    cur.execute("SELECT 1 FROM public.\"Campaignanalytics\" WHERE message_id = %s AND student_email = %s", (message_id, destinations))
                    if cur.fetchone() is None:
                        print(f"No record found with message_id {message_id} and student_email {destinations} and .")
                        return {"status": "Error", "message": f"No record found with message_id {message_id} and student_email {destinations}."}
                    else:
                        print(f"No changes made, but record exists for message_id {message_id} and student_email {destinations}.")
                else:
                    print(f"Updated record for '{event_type}' event with message_id {message_id}")

    return {"status": "Completed"}













